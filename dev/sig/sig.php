<?php

$image = imagecreatefrompng("base_sig.png");
$avatar = imagecreatefrompng("emblem.png");

//COLORS//
$android_blue = imagecolorallocate($image, 0x00, 0x99 , 0xCC);
$black = imagecolorallocate($image, 0, 0, 0);

imagefilledrectangle($image, 0, 0, 512, 72, $android_blue);
imagecopy($image, $avatar, 10, 1, 0, 0, 70, 70);
imagettftext($image, 20, 0, 80, 36, $black, "arial.ttf", "FIRE_STAR");


//output image//
header('Content-Type: image/png'); 
imagepng($image);
imagedestroy($image);
?>