<?php
  $db_host  = 'a';
  $db_name  = 'a';
  $db_user  = 'a';
  $db_pass  = 'a';
  $table_name = 'a'; 

  $base_url = 'http://ciapps.csuci.edu/directory/Search?q=Faculty&filters=Faculty&pageTarget=results&page=';
  $details_url = 'http://ciapps.csuci.edu/directory/Details/';
  $faculty_list = array();
  $debug = TRUE;

  function debug($string) {
    global $debug;

    if ($debug) {
      echo $string;
    }
  }

  function get_sql_connection($con) {
    global $db_name, $debug;

    if($con == FALSE) {
      die('<p class="fail">Cannot connect to database' . mysql_error() . '</p>');
      return FALSE;
    } 
    else {
      debug('<p class="success">Connected to database</p>');
      mysql_select_db($db_name, $con);
      return TRUE;
    }
  }

  function to_database($list, $con) {
    global $debug;
    $query = 'INSERT INTO ' . $table_name . 'values (';
    foreach ($list as $item) {
      $query = $query . $item . ',';
    }
    $query = $query . ');';

    if (!mysql_query($sql, $con)) {
      die('<p class="fail">Error: ' . mysql_error() . '</p>');
      return FALSE;
    }

    debug('<p class="success">1 record added</p>');
    return TRUE;
  }


  function curl_load($url){
    curl_setopt($ch=curl_init(), CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
  }

  function get_names($page_number = 1 ) {
    global $base_url, $faculty_list, $debug;
    $url = $base_url . strval($page_number);
    $dom = new DOMDocument();
    $html = curl_load($url);
    $dom->validateOnParse = TRUE; //<!-- this first
    $dom->loadHTML($html);        //'cause 'load' == 'parse
    $dom->preserveWhiteSpace = FALSE;
    $tr_list = $dom->getElementsByTagName('tr');
    
    foreach ($tr_list as $tr) {
      $tr = preg_replace('!\s+!', ' ', $tr->nodeValue);
      preg_match('((\w+)\.(\w+)(-(\w+))*)', $tr, $matches);
      if ($matches[0] != "") {
        $faculty_list[] = strtolower($matches[0]);
      }
    }
    debug('<p class="success">Got Names Page: ' . $page_number . '</p>');
    return TRUE;
  }

  function get_details($url) {
    global $debug;
    $dom = new DOMDocument();
    $html = curl_load($url);
    $dom->validateOnParse = TRUE; //<!-- this first
    $dom->loadHTML($html);        //'cause 'load' == 'parse
    $dom->preserveWhiteSpace = FALSE;
    $tr_list = $dom->getElementsByTagName('tr');
    $td_list = array();
    echo $tr_list;

    foreach ($tr_list->childNodes as $td) {
      echo $td->nodeValue;
      if ($td->nodeName == 'td') {
        echo $td->nodeValue;
        $td_list[] = $td;
      }
    }

/*    for ($i = 0; $i < sizeof($td_list); $i++){
      $td_list[$i] = preg_replace('!\s+!', ' ', $td_list[$i]->nodeValue);
      echo $td_list[$i];
    }*/

    debug('<p class="success">Got Details: ' . $td_list[0] . '</p>');
    return $td_list;
  }

  //$con = mysql_connect($db_host, $db_user, $db_pass);
  //if (get_sql_connection()) {
      for ($i = 1; $i <= 10; $i++) {
        get_names($i);
      }

      $faculty_url = $details_url . $faculty_list[0];
      get_details($faculty_url);
/*      foreach ($faculty_list as $faculty) {
        $faculty_url = $details_url . $faculty;
        $details_list = get_details($faculty_url);
        //to_database($details_list, $con);
      }*/

  //  mysql_close($con)
  //}

?>