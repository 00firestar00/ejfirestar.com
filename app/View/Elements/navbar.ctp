<div id="navbar" class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav navbar-left">
        <li><?php echo $this->Html->image('emblem-white.png', array('alt' => 'ejfirestar', 'style' => 'padding-top: 5px;')); ?></li>
        <li><?php echo $this->Html->link('Home', '/'); ?></li>
        <li><?php echo $this->Html->link('About', '/about'); ?></li>
        <li><?php echo $this->Html->link('Projects', '/projects'); ?></li>
        </li>
      </ul>
    </div>
  </div>
</div>
