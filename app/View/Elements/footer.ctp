<div class="container">
  <hr>
  <footer>
    <p>&copy; 2013 Evan Jarrett under a <a href="http://creativecommons.org/licenses/by/3.0/us/">Creative Commons attribution license.</a></p>
  </footer>
</div><!--/.container-->
