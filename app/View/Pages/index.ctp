<?php 
  $this->set('title_for_layout', 'Home');
?>
<style>
  body {
    padding-top: 65px;
    padding-bottom: 40px;
    background-color: #333333;
    text-align: center;
    color: #ffffff;
  }
</style>
<div class="container content">
  <div class="col-md-12"> 
    <h3>Welcome!</h3>
    <p>
      This is the home page and front end of Evan's Dev site.<br>
      This site is still in development and will continually be modified as I learn new things and have the time to implement them.<br>
      The site is currently built off the CakePHP framework along with Twitter Boostrap.<br>
      Some portions of the site, or projects may be using PhalconPHP framework.<br>
      The main design concept is to use colors and styles from the Android Design Guidlines.<br>
    </p>
  </div>
  <div id="well" class="col-md-6 col-md-offset-3 well sns-icon">
  <?php 
    echo $this->Html->image("sns_icons/google_plus.png", array(
    "alt" => "Google+",
    "width" => "64px",
    "url" => "https://google.com/+EvanJarrett"
    ));

    echo $this->Html->image("sns_icons/linked_in.png", array(
    "alt" => "LinkedIn",
    "width" => "64px",
    "url" => "http://www.linkedin.com/profile/view?id=98225064"
    ));

    echo $this->Html->image("sns_icons/bitbucket.png", array(
    "alt" => "BitBucket",
    "width" => "64px",
    "url" => "https://bitbucket.org/00firestar00"
    ));

    echo $this->Html->image("sns_icons/github.png", array(
    "alt" => "GitHub",
    "width" => "64px",
    "url" => "https://github.com/00firestar00"
    ));

    echo $this->Html->image("sns_icons/coder_wall.png", array(
    "alt" => "Coder Wall",
    "width" => "64px",
    "url" => "https://coderwall.com/00firestar00"
    ));
  ?>
  </div>
</div>