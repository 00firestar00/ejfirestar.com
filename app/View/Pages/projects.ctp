<?php
  $this->set('title_for_layout', 'Projects');
?>
<div class="container content">
  <div class="col-md-9"> 
    <h3>Projects</h3>
    <p>
      I currently have multiple projects in development. 
      Most of my projects are primarily Java based programming.</p>
    <ul>
      <li><a href="http://alltimeapp.com">AllTime</a> is an all-purpose timer and stopwatch application for Android. I'm currently working on the website, as well as some UI/UX for the application itself.</li>
      <li><a href="https://bitbucket.org/00firestar00/project1">Course Search</a> is Java application for searching course listings. I</li>
      <?php echo $this->Html->link('Git Reference', '/projects/git'); ?> is a tutorial originally required for my Software Engineering class. I have converted it into a nicely formatted web page.</li>
    </ul>
  </div>
  <div class="col-md-3">
    <?php echo $this->element('sidebar'); ?>
  </div>
</div>
