<?php
  $this->set('title_for_layout', 'Git Tutorial');
?>
<div class="container">
  <div class="col-md-9">
    <div class="hero-unit">
      <h1>Git Reference Sheet</h1>
      <p>A reference guide for using Git. This shows some of the more commonly used commands. 
        For more information, you should check out the<a href="http://git-scm.com/documentation"> Official Documentation</a>.
      </p>
    </div>
    <section id="new_project">
      <div class="page-header">
        <h1>Creating a new Project</h1>
      </div>
      <p>
        When beginning a new project you will first want to navigate to the directory of that project. 
        Then you are going to need to initialize the project. 
        Once initialized you can set up remote connection to repositories, commit changes, and set up a way to ignore files. 
        If you are simply cloning a repository, check the <a href="#">Cloning a Remote Repo</a> section first. 
        For more on <code>.gitignore</code> see the <a href="#">Ignoring Files</a> section.
      </p>
      <p>
        Navigate to your project using <code>cd</code>. You only want to navigate to the root directory. 
        That is, <strong>Do Not</strong> navigate into other directories such as <code>src/</code>.
        <code>$ cd path/to/your/project</code>
      </p>
      <p>
        Once you are in the directory of your project, you can use:
        <code>$ git init</code>
      </p>
      <p>
        If you messed up the init, or want to start from scratch and need to remove it. you can use:
        <code>$ rm -rf .git</code>
      </p>
      <p>
        Be careful with this command and make sure you are in the correct directory before attempting this. 
        Alternatively you can use a file explorer to delete it. 
        Make sure you can see hidden files in order to delete via the file explorer.
      </p>
      <p>
        Now you need to link it to a remote repository. If you are using <a href="https://bitbucket.org/">Bitbucket</a>, it will guide you through the process. 
        Here is a sample of how to add a repository: 
        <code>$ git remote add origin ssh://git@bitbucket.org/00firestar00/testrepo.git</code>
      </p>
      <span class="label label-info">Note: The above example uses SSH connections. While recommended, SSH is not needed.</span>
    </section>
  </div>
  <div class="col-md-3">
    <?php echo $this->element('sidebar'); ?>
  </div>
</div>
