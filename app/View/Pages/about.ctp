<?php
  $this->set('title_for_layout', 'About');
?>
<div class="container content">
  <div class="col-md-9"> 
    <h3>About Me</h3>
    <p>
      I'm currently a student at CSU Channel Islands studying in Computer Science. In my spare time I focus on coding projects. 
      Since my primary focus is on Java based programming, I have worked on Android Apps, Bukkit Plugins for Minecraft as well as other odds and ends projects.
      I also have some basic experience in web development as you can see from this website. I'm not the best at it, but I know enough to get by with basic PHP and CSS.
      In addition to all that I have worked with mySQL and SQLite databases for a few projects. I'm not simply all about code. 
      I can use <a href="http://www.gimp.org/">GIMP</a> quite proficiently and have work with 3D modeling and CAD based programs as well.
      <br><br>
      You can checkout my resume <a href="/files/EvanJarrett_Resume.pdf">here!</a>
    </p>
  </div>
  <div class="col-md-3">
    <?php echo $this->element('sidebar'); ?>
  </div>
</div>
