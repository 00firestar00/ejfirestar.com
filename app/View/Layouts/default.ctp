<!DOCTYPE html>
<head>
  <meta charset="utf-8">

  <meta content='summary' name='twitter:card'/>
  <meta content='@ejfirestar' name='twitter:site'/>
  <meta content='Evan Jarrett' property='og:title'/>
  <meta content='website' property='og:type'/>
  <meta content='http://ejfirestar.com/' property='og:url'/>
  <meta content='Evan Jarrett' property='og:site_name'/>
  <meta content='Personal webpage for Evan Jarrett.' name='og:description'/>

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>Evan Jarrett<?php echo isset($title_for_layout)?' &middot; ' . $title_for_layout:''; ?></title>

  <meta name="language" content="EN">
  <meta name="copyright" content="ejfirestar.com All rights reserved.">

  <meta name="description" content="<?php echo isset($description)?$description:'Personal webpage for Evan Jarrett. Used for testing, development and work showcase'; ?>">
  <meta name="keywords" content="Minecraft Server, Java, Dev, Android, Tech, Programming <?php echo isset($additional_keywords)?$additional_keywords:''; ?>">

  <meta name="robots" content="index, follow" />
  <meta name="revisit-after" content="1 Day" />

  <style>
        body {
            padding-top: 65px;
            padding-bottom: 40px;
        }
  </style>

  <?php
    echo $this->fetch('meta');
    echo $this->Html->meta('icon');
    echo $this->Html->css('bootstrap.min');
    echo $this->Html->css('bootstrap-responsive.min');
    echo $this->Html->css('main');
    echo $this->Html->script('/js/jquery-1.10.1.min.js', array('inline' => false));
    echo $this->Html->script('/js/bootstrap.min.js', array('inline' => false));
    echo $this->Html->script('/js/modernizr-2.6.2.min.js', array('inline' => false));
    echo $this->Html->script('/js/main.js', array('inline' => false));
    echo $this->fetch('css');
  ?>
<link href='http://fonts.googleapis.com/css?family=Prosto+One|Raleway|Monda' rel='stylesheet' type='text/css'>
</head>
<body>
  <?php echo $this->element('navbar'); ?>
  <?php echo $this->Session->flash(); ?>
  <?php echo $this->fetch('content'); ?>
  <?php echo $this->element('footer'); ?>
<script>
    var _gaq=[['_setAccount','UA-39711309-1'],['_trackPageview']];
    (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
    g.src='//www.google-analytics.com/ga.js';
    s.parentNode.insertBefore(g,s)}(document,'script'));
</script>
<?php echo $this->fetch('script'); ?>
</body>
</html>
